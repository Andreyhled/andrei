import _ from 'lodash';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import './animation/livedemo.js';
import Animation from './animation/bezier.js';

import './css/code.css';
import './css/footer.css';
import code from './css/index.css';

// import png from './Red.png';
import WelcomePart from './components/welcome.html';
import InfoPart from './components/info.html';

const welcome = document.createElement('div');
welcome.className = "welcome container-fluid";
welcome.id = "welcome";
welcome.innerHTML = WelcomePart;
document.body.appendChild(welcome);

const info = document.createElement('div');
info.className = "info container-fluid";
info.id = "info";
info.innerHTML = InfoPart;
welcome.appendChild(info);


