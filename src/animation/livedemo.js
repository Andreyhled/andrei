document.addEventListener('DOMContentLoaded', function(){
	// Элемент demo
	const livedemo = document.getElementById('demo');

	class BorderStyle {
		constructor() {
			this.input  = document.getElementById('bor-width');
			this.dotted = document.getElementById('dotted');
			this.dashed = document.getElementById('dashed');
			this.solid  = document.getElementById('solid');
			this.double = document.getElementById('double');
			this.groove = document.getElementById('groove');
			this.ridge  = document.getElementById('ridge');
			this.inset  = document.getElementById('inset');
			this.outset = document.getElementById('outset');
			this.black = document.getElementById('black');
			this.blue = document.getElementById('blue');
			this.red = document.getElementById('red');
			this.width  = 0;
			this.style  = "solid";
			this.color  = "black";

			this.input.oninput = () => {
				if (this.input.value > 10) {
					alert("Нельзя ставить значение больше 10px");
					this.input.value = 10;
				}
				this.width = this.input.value;
				this.UpdateBorder();
			};

			this.dotted.onclick = () =>  {
				this.style = this.dotted.id;
				this.UpdateBorder();
				// console.log("got it");
			}

			this.dashed.onclick = () =>  {
				this.style = this.dashed.id;
				this.UpdateBorder();
			}

			this.solid.onclick = () =>  {
				this.style = this.solid.id;
				this.UpdateBorder();
			}

			this.double.onclick = () =>  {
				this.style = this.double.id;
				this.UpdateBorder();
			}

			this.groove.onclick = () =>  {
				this.style = this.groove.id;
				this.UpdateBorder();
			}

			this.ridge.onclick = () =>  {
				this.style = this.ridge.id;
				this.UpdateBorder();
			}

			this.inset.onclick = () =>  {
				this.style = this.inset.id;
				this.UpdateBorder();
			}

			this.outset.onclick = () =>  {
				this.style = this.outset.id;
				this.UpdateBorder();
			}

			this.black.onclick = () =>  {
				this.color = this.black.id;
				this.UpdateBorder();
			}

			this.blue.onclick = () =>  {
				this.color = this.blue.id;
				this.UpdateBorder();
			}

			this.red.onclick = () =>  {
				this.color = this.red.id;
				this.UpdateBorder();
			}
			
		}

		init() {
			this.CreateBorder();
		}
		
		CreateBorder() {
			livedemo.style.border = this.width + 'px ' + this.style + ' ' + this.color;
		}

		UpdateBorder() {
			this.CreateBorder();
		}
	};

	new BorderStyle().init();

	// Scroll to liveDemo 
	let demoholder = document.getElementById('info-rule');
	let scroll =  document.getElementById('scroll');
	scroll.onclick = () => {
		demoholder.scrollIntoView({behavior: 'smooth'});
		console.log('scrolling');
	}
	

});