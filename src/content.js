import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
// import png from './Red.png';
//import animation from './animation/bezier.js';
import ContentPage from './components/content.html';
import './content.css';
const page = document.createElement('div');
page.className = "window container-fluid";
page.id = "content";
page.innerHTML = ContentPage;

document.body.appendChild(page);
