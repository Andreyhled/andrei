const path = require('path');
var HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    welcome: path.join(__dirname,'src','index.js'),
    content: path.join(__dirname,'src','content.js'),
  },
  output: {
    filename: '[name].main.js',
    path: path.resolve(__dirname, 'dist')
  },

  plugins: [
   new HtmlWebPackPlugin({
       filename: './index.html',
       chunks: ['welcome']
    }),

   new HtmlWebPackPlugin({
       filename: './content.html',
       chunks: ['content']
    })
  ],

  module: {
  	rules: [
  		{
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
  		],
  		},
      // {
      //   test: /\.html$/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         name: '[name].[ext]'
      //       }
      //     }
      // ],
      //   //exclude: path.resolve(__dirname,'src/public/index.html')
      // },

  		{
        test: /\.css$/,
        use: [
           'style-loader',
           'css-loader',
        ],
       	},

       	{
        test: /\.(png|svg|jpg|gif)$/,
        use: [
           'file-loader',
        ],
       },

       {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
           'file-loader',
        ],
       },
  	
  	]
  },

};